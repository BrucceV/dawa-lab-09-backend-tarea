
//Puerto
process.env.PORT = process.env.PORT || 3000;

//Entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

//Vencimiento de Token
//60 seg * 60 min * 24 h * 30 d
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30

//Seed de autentificacion
process.env.SEED = process. env.SEED || 'este-es-es-seed-desarrollo'


//Base de Datos
let urlDB;

if(process.env.NODE_ENV === 'dev' ){
    urlDB = 'mongodb://localhost:27017/lab08'
}else{
    urlDB = 'mongodb+srv://brucce:UtTmjKg8p4.ifZt@cluster0-jp56b.mongodb.net/lab08'
}

process.env.URLDB = urlDB