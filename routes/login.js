const express = require("express");

//const bcrypt = require("bcrypt");

import bcrypt from "bcrypt";

var jwt = require('jsonwebtoken');

import Usuario from "../models/usuario";

const app = express();

app.post("/login", async(req, res) => {

    const {email, password} = req.body;

    console.log("Este es solo email ", email);
    console.log("Este es solo password ", password + ' mis huveos') ;

try{
    const usuarioDB = await Usuario.findOne({ email: email});
    console.log("Este es solo usuarioDB ", usuarioDB);
        if(!usuarioDB){
            return res.status(401).json({
                ok: false,
                err:{
                    message: "(Usuario) o contraseña incorrectas",
                },
            });
        }
        console.log(bcrypt.compareSync(password, usuarioDB.password))
        console.log(password.length)
        if(!bcrypt.compareSync(password, usuarioDB.password)) {
            return res.status(402).json({
                ok: false,
                err: {
                    message: "Usuario o (contraseña) incorrectas",
                },
            });
        }
        const token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN})
        return res.json({
            ok: true,
            usuario: usuarioDB,
            token: token,
        })
    }catch(err){
        console.log("Error 500: " + err)
        res.status(500).json({
            ok: false,
            message: "Error catch",
            err,
        })
    }

});

module.exports = app;