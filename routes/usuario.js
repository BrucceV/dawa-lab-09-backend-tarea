import express from 'express';
const bcrypt = require('bcrypt');
const _ = require('underscore');

//const app = express();
const bodyParser = require("body-parser");

const app = express.Router();

// importar el modelo Usuario
import Usuario from "../models/usuario";

const {verificaToken} = require('../middlewares/authetication');


//create application/x-ww-form-urlencoded parser
app.use(bodyParser.urlencoded({extended: true}));

app.post("/usuario", async(req, res) => {
    const body = req.body;

    try{
        const usuarioDB = await Usuario.create({
            nombre: body.nombre,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10),
            /* password: body.password ? bcrypt.hashSync(body.password, 10) : null, */
            role: body.role,
        });
        usuarioDB.password = null;
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });
    }catch (err) {
        return res.status(500).json({
            ok: false,
            err,
        })
    }
});

app.get('/usuario/:id', async(req, res) => {
    const _id = req.params.id;
    try {
      const usuarioDB = await Usuario.findOne({_id});
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });
    }catch (err) {
        return res.status(400).json({
            ok: false,
            err,
        })
    }
});

app.get("/usuario", verificaToken ,async(req, res) => {
    try{
        const usuarioDB = await Usuario.find();
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });
    } catch(err){
        return res.status(400).json({
            ok: false,
            err,
        })
    }
});

app.put("/usuario/:id", async (req, res) => {
    const _id = req.params.id;
    const body = _.pick(req.body, ['nombre','email','password','role','estado']);

    if(body.password){
        //body.password ? body.password = bcrypt.hashSync(req.body.password, 10) : null;
        body.password = bcrypt.hashSync(req.body.password, 10);
    }

    try{
        const usuarioPut = await Usuario.findByIdAndUpdate(
            _id,
            body,
            {new: true}
        );
        res.status(200).json({
            ok: true,
            usuario: usuarioPut,
        });
    }catch(err){
        return res.status(400).json({
            ok: false,
            err,
        })
    }
});


app.delete("/usuario/:id", async (req, res) => {
    const _id = req.params.id;

    try{
        const usuarioDelete = await Usuario.findByIdAndDelete({_id});
        if(!usuarioDelete){
            return res.status(400).json({
                ok: false,
                mensaje: 'No se encontro el id',
                err,
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDelete,
        });

    }catch(err){
        return res.status(400).json({
            ok: false,
            err,
        })
    }
});
//Exportar el router
module.exports = app;