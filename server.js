import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import path from 'path';
//const express = require("express");
//const morgan = require('morgan');
//const cors = require('cors');
//const path = require('path');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require("body-parser");
//const userRouter = require('./routes/usuario');
require('./config/config');

//configuracion puerto
//app.set('port', process.env.PORT || 3000);

// middlewares
    //create application/x-ww-form-urlencoded parser
app.use(bodyParser.urlencoded({extended: true}));
    //create application/jaon parser
app.use(bodyParser.json());
    //Nos sirve para pintar las peticiones HTTP request que se solicitan a nuestro aplicación.
app.use(morgan('tiny'));
    //Para realizar solicitudes de un servidor externo e impedir el bloqueo por CORS
app.use(cors());

//routes
//app.use('/', userRouter);
app.use(require("./routes/index"));

// Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

const uri = 'mongodb://localhost:27017/lab07Tarea';
const uri2 = process.env.URLDB;

const options = { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true};

//Conexion a DB usando promesas
mongoose.connect(uri2, options).then(
    /** listo para usar. La promesa `mongoose.connect ()` se resuelve en la instancia de mongoose. */
    () => {
        console.log('Conectado a tu marrana Base de Datos')
    },
    /** manejar error de conexión inicial */
    err => { console.log(err) }
  );

//server listen
app.listen(process.env.PORT, () => {
    console.log(`Escuchando tus marranas peticiones en el puerto ${process.env.PORT}`);
  });

